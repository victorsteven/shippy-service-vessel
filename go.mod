module gitlab.com/victorsteven/shippy-service-vessel

go 1.13

require (
	github.com/golang/protobuf v1.3.5
	github.com/micro/go-micro v1.18.0
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
)
